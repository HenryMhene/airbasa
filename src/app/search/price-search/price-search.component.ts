import { Component, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';
import { TaskService } from 'src/app/task/task.service';
@Component({
  selector: 'app-price-search',
  templateUrl: './price-search.component.html',
  styleUrls: ['./price-search.component.css']
})
export class PriceSearchComponent implements OnInit {
  Price: any;

  constructor(  public dialogRef: MatDialogRef<PriceSearchComponent>,   private taskService: TaskService) { }

  ngOnInit() {
  }
  async cancel() {
    this.dialogRef.close();
  }

  async apply() {
    await this.taskService.searchbyprice(this.Price).subscribe(data => {
      // do stuff with our data here.
      if (data) {
        const term = this.Price;
        const results = [data, term];
        this.dialogRef.close(results);
      } else {
        this.dialogRef.close();
      }
    });
  }
}
