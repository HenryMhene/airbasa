import { Component, OnInit } from '@angular/core';
import { GooglePlaceDirective } from 'ngx-google-places-autocomplete';
import { ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';
import { TaskService } from 'src/app/task/task.service';
@Component({
  selector: 'app-location-search',
  templateUrl: './location-search.component.html',
  styleUrls: ['./location-search.component.css']
})
export class LocationSearchComponent implements OnInit {
  countryOptions: any;
  @ViewChild('placesRef') placesRef: GooglePlaceDirective;
  location: any;
  constructor(  public dialogRef: MatDialogRef<LocationSearchComponent>,   private taskService: TaskService) { }

  ngOnInit() {
    this.countryOptions = {
      bounds: null,
      componentRestrictions: {
        country: ['BW', 'NA', 'ZA', 'ZW']
      },
      types: ['(regions)']
    };
  }
  async cancel() {
    this.dialogRef.close();
  }

  async apply() {
    if (this.location) {
    await this.taskService.searchbylocation(this.location).subscribe(data => {
      // do stuff with our data here.
      if (data) {
        const term = this.location;
        const results = [data, term];
        this.dialogRef.close(results);
      } else {
        this.dialogRef.close();
      }
    });
    }

  }

  handleAddressChange(data): void {
    this.location = data.formatted_address;
  }

}
