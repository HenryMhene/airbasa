import { Component, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';
import { TaskService } from 'src/app/task/task.service';
@Component({
  selector: 'app-task-type-search',
  templateUrl: './task-type-search.component.html',
  styleUrls: ['./task-type-search.component.css']
})
export class TaskTypeSearchComponent implements OnInit {
  hide = false;
  constructor(  public dialogRef: MatDialogRef<TaskTypeSearchComponent>,   private taskService: TaskService) { }

  ngOnInit() {
  }
  async cancel() {
    this.dialogRef.close();
  }

  async apply() {
    if (this.hide) {
    await this.taskService.searchbytasktype('Open').subscribe(data => {
      // do stuff with our data here.
      if (data) {
        const term = 'Open';
        const results = [data, term];
        this.dialogRef.close(results);
      } else {
        this.dialogRef.close();
      }
    });
    }

  }
}
