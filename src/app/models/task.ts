import { Resource } from './Resource';
export class Task extends Resource {
    CreatorId: string;
    CreatorName: string;
    CreatorPicture: string;
    Summary: string;
    Description: string;
    Price: number;
    TaskLocation: string;
    DueDate: string;
    TaskStatus: string;
    Category: string;
    NumberOfOffers: String;
    Active: string;
    AssignedUser: string;
    MustHaves: Array<string>;
    CreatedAt: string | any;
}
