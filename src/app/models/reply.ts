import { Resource } from './Resource';
export class Reply extends Resource {
    OfferId: string;
    QuestionId: string;
    CreatorId: string;
    CreatorName: string;
    CreatorPicture: string;
    Summary: string;
    Active: string;
    CreatedAt: string;
    TempCreatedAt?: string;
}
