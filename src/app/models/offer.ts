import { Resource } from './Resource';
export class Offer extends Resource {
    TaskId: string;
    CreatorId: string;
    CreatorName: string;
    CreatorPicture: string;
    Amount: number;
    ServiceFee: number;
    Receivable: number;
    Summary: string;
    Active: string;
    Status: string;
    CreatedAt: string;
    TempCreatedAt?: string;
}
