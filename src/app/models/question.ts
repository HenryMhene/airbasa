import { Resource } from './Resource';
export class Question extends Resource {
    TaskId: string;
    CreatorId: string;
    CreatorName: string;
    CreatorPicture: string;
    Summary: string;
    Active: string;
    CreatedAt: string;
    TempCreatedAt?: string;
}
