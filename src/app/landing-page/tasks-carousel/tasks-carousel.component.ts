import { Component, OnInit, Input } from '@angular/core';
import { NguCarousel } from '@ngu/carousel';
import { TaskService } from '../../task/task.service';
import { QueryOptions } from '../../task/query.options';
import {CreateComponent } from '../../task/create/create.component';
import { LoginComponent } from '../../login/login.component';
import { MatDialog } from '@angular/material';
@Component({
  selector: 'app-tasks-carousel',
  templateUrl: './tasks-carousel.component.html',
  styleUrls: ['./tasks-carousel.component.scss']
})
export class TasksCarouselComponent implements OnInit {
  public carouselOptions: NguCarousel;
  tasks: any;
  user_id_token = localStorage.getItem('id_token');
  user_access_token = localStorage.getItem('access_token');
  constructor(private taskService: TaskService, private queryOptions: QueryOptions,  public dialog: MatDialog) {
    this.loadTasks();
  }

  async loadTasks() {
    this.queryOptions.pageNumber = 1;
    this.queryOptions.pageSize = 100;
    await this.taskService.list(this.queryOptions).subscribe(data => this.tasks = data);

  }

  viewTask(task) {
    localStorage.setItem('selected_browse_task', btoa(JSON.stringify(task)));
    localStorage.setItem('last_page', '/tasks/' + task.Id);
  }

  ngOnInit() {
    this.carouselOptions = {
      grid: { xs: 1, sm: 2, md: 3, lg: 3, all: 0 },
      slide: 2,
      speed: 400,
      interval: 4000,
      point: {
        visible: true
      },
      load: 2,
      touch: true,
      loop: true
    };
  }

  openTaskCreate(): void {

    if (this.user_access_token && this.user_id_token) {
      const dialogRef = this.dialog.open(CreateComponent, {
        width: '500px',
         data: {name: 'test', animal: 'test'}
      });
      dialogRef.afterClosed().subscribe(result => {
        console.log('The dialog was closed');
      });
    } else {
      const dialogRef = this.dialog.open(LoginComponent, {
         data: {name: 'test', animal: 'test'}
      });
      dialogRef.afterClosed().subscribe(result => {
        console.log('The dialog was closed');
      });

    }


  }

}
