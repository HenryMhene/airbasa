import { Component, OnInit } from '@angular/core';
import {MatDialog} from '@angular/material';
import {CreateComponent } from '../../task/create/create.component';
import { LoginComponent } from '../../login/login.component';
@Component({
  selector: 'app-intro',
  templateUrl: './intro.component.html',
  styleUrls: ['./intro.component.scss']
})
export class IntroComponent implements OnInit {
  user_id_token = localStorage.getItem('id_token');
  user_access_token = localStorage.getItem('access_token');
  constructor(  public dialog: MatDialog) { }

  ngOnInit() {
  }

  openTaskCreate(taskType): void {

    if (this.user_access_token && this.user_id_token) {
      const dialogRef = this.dialog.open(CreateComponent, {
        width: '500px',
         data: {name: 'test', animal: 'test'}
      });
      dialogRef.afterClosed().subscribe(result => {
        console.log('The dialog was closed');
      });
    } else {
      const dialogRef = this.dialog.open(LoginComponent, {
         data: {name: 'test', IncomingUrl: '/landing'}
      });
      dialogRef.afterClosed().subscribe(result => {
        console.log('The dialog was closed');
      });

    }


  }

}
