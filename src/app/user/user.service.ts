import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class UserService {
  Url = 'assets/users.json';
  constructor(private http: HttpClient) { }

  async getUsers() {
    return await this.http.get(this.Url).toPromise();
  }
}
