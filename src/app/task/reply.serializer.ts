import { Offer } from '../models/offer';
import { MomentModule, DateFormatPipe } from 'ngx-moment';
import { Reply } from '../models/reply';
export class ReplySerializer {
    fromJson(json: any): Reply {
      const reply = new Reply();
      reply.Id = json.Id;
      reply.OfferId = json.OfferId;
      reply.QuestionId = json.QuestionId;
      reply.CreatorId = json.CreatorId;
      reply.CreatorName = json.CreatorName;
      reply.CreatorPicture = json.CreatorPicture;
      reply.Summary = json.Summary;
      reply.Active = json.Active;
      reply.CreatedAt = json.CreatedAt;
      const createdDate = new Date(json.CreatedAt * 1000);
      reply.TempCreatedAt = (new DateFormatPipe()).transform(createdDate, 'YYYY-MM-DD HH:mm');
      return reply;
    }

    toJson(reply: Reply): any {

      let CreatedAt: any;

      CreatedAt = reply.CreatedAt;
      if (!reply.Id || !reply.CreatedAt) {
        CreatedAt = Math.floor(Date.now() / 1000);
      }

      if (!reply.QuestionId) {
        reply.QuestionId = 'Null';
      }
      if (!reply.OfferId) {
        reply.OfferId = 'Null';
      }
      return {
        Id: reply.Id,
        OfferId: reply.OfferId,
        QuestionId: reply.QuestionId,
        CreatorId: reply.CreatorId,
        CreatorName: reply.CreatorName,
        CreatorPicture: reply.CreatorPicture,
        Summary: reply.Summary,
        Active: reply.Active,
        CreatedAt: CreatedAt,
      };
    }
  }
