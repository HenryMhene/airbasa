import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormBuilder, FormGroup, Validators, FormControl, FormGroupDirective, NgForm } from '@angular/forms';
import { Task } from '../../models/task';
import { TaskService } from '../task.service';
import { v4 as uuid } from 'uuid';
import { MatSnackBar } from '@angular/material';
import { GooglePlaceDirective } from 'ngx-google-places-autocomplete';
import { ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import {ErrorStateMatcher} from '@angular/material/core';
import {ChangeDetectorRef, OnDestroy} from '@angular/core';
import {MediaMatcher} from '@angular/cdk/layout';
export interface DialogData {
  animal: string;
  name: string;
  summary: string;
}
/** Error when invalid control is dirty, touched, or submitted. */
export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}
@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateComponent implements OnInit {
  priceFormControl = new FormControl('', [
    Validators.required, Validators.min(1), Validators.max(50000)
  ]);
  mobileQuery: MediaQueryList;
  private _mobileQueryListener: () => void;
  matcher = new MyErrorStateMatcher();
  user_name = localStorage.getItem('profile_name');
  user_picture = localStorage.getItem('profile_picture');
  user_id_token = localStorage.getItem('id_token');
  user_access_token = localStorage.getItem('access_token');
  user_sub = localStorage.getItem('profile_sub');
  isLinear = true;
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  thirdFormGroup: FormGroup;
  fourthFormGroup: FormGroup;
  fifthFormGroup: FormGroup;
  dueDateOption: string;
  selectDate: string;
  disableDatepicker = true;
  model = new Task();
  currentDatetime: any;
  category = 'Anything';
  duedate: any;
  loading = false;
  countryOptions: any;
  // countryOptions: any;
  @ViewChild('placesRef') placesRef: GooglePlaceDirective;
  constructor(private _formBuilder: FormBuilder,
    public dialogRef: MatDialogRef<CreateComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    private taskService: TaskService,
    public snackBar: MatSnackBar,
    public router: Router,
    changeDetectorRef: ChangeDetectorRef,
    media: MediaMatcher,
  ) {
    this.mobileQuery = media.matchMedia('(max-width: 685px)');
    this._mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addListener(this._mobileQueryListener);
  }

  ngOnInit() {

    this.countryOptions = {
      bounds: null,
      componentRestrictions: {
        country: ['BW', 'NA', 'ZA', 'ZW']
      },
      types: ['(regions)']
    };
    if (this.data.summary) {
      this.model.Summary = this.data.summary;
    }
    this.currentDatetime = Math.floor(Date.now() / 1000);

    // by default due date should be today
    this.model.DueDate = String(this.currentDatetime);

    this.firstFormGroup = this._formBuilder.group({
      firstCtrl: ['', Validators.required]
    });
    this.secondFormGroup = this._formBuilder.group({
      secondCtrl: ['', Validators.required]
    });
    this.thirdFormGroup = this._formBuilder.group({
      thirdCtrl: ['', Validators.required],
    });
    this.fourthFormGroup = this._formBuilder.group({
      fourthCtrl: ['', Validators.required],
      datePickerCtrl: [{ value: '', disabled: this.disableDatepicker }]
    });
    this.fifthFormGroup = this._formBuilder.group({
      fifthCtrl: ['', [Validators.required, Validators.min(1), Validators.max(50000)]],
    });

    // matcher = new MyErrorStateMatcher();
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  async save() {
    this.loading = true;
    this.model.Id = uuid();
    this.model.CreatorName = this.user_name;
    this.model.CreatorId = this.user_sub;
    this.model.CreatorPicture = this.user_picture;
    this.model.Category = this.category;
    this.model.TaskStatus = 'Open';
    this.model.Active = 'true';
    this.model.AssignedUser = '';
    // this.model.DueDate = this.currentDatetime.toString;
    this.model.CreatedAt = String(this.currentDatetime);

    await this.taskService.create(this.model).subscribe(data => {
      this.snackBar.open('Task Created!', 'Close', {
        duration: 2000,
      });
      this.loading = false;
      this.dialogRef.close();
      this.router.navigate(['/my-tasks']);
    });
  }


  radioChange(): void {
    // enable date selection option if option 2 selected
    if (this.dueDateOption === '2') {
      this.disableDatepicker = false;
    } else if (this.dueDateOption === '1') {
      // try get end of day
      this.model.DueDate = String(this.currentDatetime);
      this.disableDatepicker = true;
    } else if (this.dueDateOption === '3') {
      // try get end of week + 7 days
      const withinoneweek = new Date();
      withinoneweek.setDate(withinoneweek.getDate() + 7);
      const unixdate = new Date(withinoneweek).getTime() / 1000;
      this.model.DueDate = String(unixdate);
      this.disableDatepicker = true;
    }
  }

  dateChange(data): void {
    this.duedate = new Date(data.value).getTime() / 1000;
    // enable date selection option if option 2 selected
    this.model.DueDate = String(this.duedate);
  }

  handleAddressChange(data): void {
    this.model.TaskLocation = data.formatted_address;
  }
}
