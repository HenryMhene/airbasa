import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { ResourceService } from './resource.service';
import { Task } from '../models/task';
import { TaskSerializer } from './task.serializer';
@Injectable({
  providedIn: 'root'
})
  export class TaskService extends ResourceService<Task> {
    constructor(httpClient: HttpClient) {
      super(
        httpClient,
        environment.apiUrl,
        'tasks',
        new TaskSerializer());
    }
  }
