import { Component, OnInit, Inject } from '@angular/core';
import { Task } from 'src/app/models/task';
import { Offer } from 'src/app/models/offer';
import { Question } from 'src/app/models/question';
import { MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';
import { Reply } from 'src/app/models/reply';
import { v4 as uuid } from 'uuid';
import { ReplyService } from '../reply.service';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { MediaMatcher } from '@angular/cdk/layout';
import { MomentModule, DateFormatPipe } from 'ngx-moment';
import { ChangeDetectorRef } from '@angular/core';
export interface DialogData {
  task: Task;
  model: Offer|Question;
}

@Component({
  selector: 'app-reply',
  templateUrl: './reply.component.html',
  styleUrls: ['./reply.component.css']
})
export class ReplyComponent implements OnInit {
  mobileQuery: MediaQueryList;
  private _mobileQueryListener: () => void;
  loading = true;
  chatType: string;
  model: Offer|Question;
  user_name = localStorage.getItem('profile_name');
  user_picture = localStorage.getItem('profile_picture');
  user_id_token = localStorage.getItem('id_token');
  user_access_token = localStorage.getItem('access_token');
  user_sub = localStorage.getItem('profile_sub');
  currentDatetime: any;
  reply = new Reply();
  replyFormGroup: FormGroup;

  replyFormControl = new FormControl('', [
    Validators.required
  ]);

  constructor(   public dialogRef: MatDialogRef<ReplyComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData, private replyService: ReplyService, public snackBar: MatSnackBar
    , private _formBuilder: FormBuilder, media: MediaMatcher ,
    changeDetectorRef: ChangeDetectorRef) {
      this.mobileQuery = media.matchMedia('(max-width: 600px)');
      this._mobileQueryListener = () => changeDetectorRef.detectChanges();
      this.mobileQuery.addListener(this._mobileQueryListener);
    }

  ngOnInit() {
    this.chatType = this.data.model.constructor.name;
    this.model = this.data.model;
    console.log(this.model);
    this.currentDatetime = Math.floor(Date.now() / 1000);
    // setTimeout(() => {
      this.loading = false;
    // }, 500);
  }

  async saveReply() {
    this.loading = true;
    this.reply.Id = uuid();
    if (this.chatType === 'Offer') {
      this.reply.OfferId = this.model.Id;
    } else if ( this.chatType === 'Question') {
      this.reply.QuestionId = this.model.Id;
    }
    this.reply.CreatorName = this.user_name;
    this.reply.CreatorId = this.user_sub;
    this.reply.CreatorPicture = this.user_picture;
    this.reply.Active = 'true';
    this.reply.CreatedAt = String(this.currentDatetime);

    await this.replyService.create(this.reply).subscribe(data => {
      this.snackBar.open('Reply sent!', 'Close', {
        duration: 2000,
      });
      setTimeout(() => {
        this.loading = false;
        this.dialogRef.close();
      }, 500);
    });
  }

}
