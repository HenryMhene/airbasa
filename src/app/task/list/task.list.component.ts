import { Component, OnInit, Input } from '@angular/core';
import { ChangeDetectorRef, OnDestroy } from '@angular/core';
import { MediaMatcher } from '@angular/cdk/layout';
import { MatDialog } from '@angular/material';
import { TaskService } from '../task.service';
import { Task } from '../../models/task';
import { QueryOptions } from '../query.options';
import { ActivatedRoute, Router } from '@angular/router';
import { OfferService } from '../offer.service';
import { LocationSearchComponent } from 'src/app/search/location-search/location-search.component';
import { PriceSearchComponent } from 'src/app/search/price-search/price-search.component';
import { TaskTypeSearchComponent } from 'src/app/search/task-type-search/task-type-search.component';

@Component({
  selector: 'app-task-list',
  templateUrl: './task.list.component.html',
  styleUrls: ['./task.list.component.css']
})
export class TaskListComponent implements OnInit, OnDestroy {
  mobileQuery: MediaQueryList;
  user_name = localStorage.getItem('profile_name');
  user_picture = localStorage.getItem('profile_picture');
  user_id_token = localStorage.getItem('id_token');
  user_access_token = localStorage.getItem('access_token');
  user_sub = localStorage.getItem('profile_sub');
  private _mobileQueryListener: () => void;
  tasks: any;
  showTaskDetails = false;
  showAssignedTasksDetails = false;
  showPostedTasksDetails = false;
  showDraftTasksDetails = false;
  showPendingTasksDetails = false;
  showCompletedTasksDetails = false;
  selectedTask: Task;
  tasksAvailable = false;
  assignedTasksAvailable = false;
  postedTasksAvailable = false;
  draftTasksAvailable = false;
  pendingTasksAvailable = false;
  completedTasksAvailable = false;
  mytasksview = false;
  draftTasks: any;
  assignedTasks: any;
  postedTasks: any;
  pendingTasks: any;
  completedTasks: any;
  loadView = false;
  routerLinkBase: any;
  searchSummary: string;
  searchOn = false;
  locationLabel = 'Location';
  priceLabel = 'Price';
  tasktypeLabel: string;
  dialogtop: any;
  dialogleft: any;
  toggle = true;
  mobiletaskselected = false;
  constructor(
    public dialog: MatDialog,
    changeDetectorRef: ChangeDetectorRef,
    media: MediaMatcher,
    private taskService: TaskService,
    private offerService: OfferService,
    private queryOptions: QueryOptions,
    public router: Router

  ) {
    this.mobileQuery = media.matchMedia('(max-width: 600px)');
    this._mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addListener(this._mobileQueryListener);
    if (localStorage.getItem('search_on')) {
      this.searchOn = JSON.parse(atob(localStorage.getItem('search_on')));
    } else {
      this.searchOn = false;
    }

    if (localStorage.getItem('selected_browse_task') && this.mobileQuery.matches) {
      this.mobiletaskselected = true;
    }

    if (this.router.url.indexOf('/my-tasks') === 0) {
      this.mytasksview = true;
      localStorage.removeItem('selected_browse_task');
      this.loadAssignedTasks();
      this.loadPostedTasks();
      this.loadPendingTasks();
      this.routerLinkBase = 'my-tasks';
    } else if ((this.router.url.indexOf('/tasks') === 0) && (this.searchOn === false)) {
      this.loadBrowseTasks();
      this.mytasksview = false;
      this.routerLinkBase = 'tasks';
    } else if ((this.searchOn === true)) {
      this.mytasksview = false;
      this.routerLinkBase = 'tasks';
      if (localStorage.getItem('last_search_term')) {
        this.searchSummary = JSON.parse(atob(localStorage.getItem('last_search_term')));
      }
      if (localStorage.getItem('last_search_location')) {
        this.locationLabel = JSON.parse(atob(localStorage.getItem('last_search_location')));
      }
      if (localStorage.getItem('last_search_price')) {
        this.priceLabel = JSON.parse(atob(localStorage.getItem('last_search_price')));
      }
      if (localStorage.getItem('last_search_results')) {
        this.tasks = JSON.parse(atob(localStorage.getItem('last_search_results')));
        if (this.tasks) {
          if (localStorage.getItem('selected_browse_task')) {
            this.selectedTask = JSON.parse(atob(localStorage.getItem('selected_browse_task')));
          } else {
            this.selectedTask = this.tasks['0'];
          }
          this.showTaskDetails = true;
          this.tasksAvailable = true;
        }
      } else {
        this.searchOn = false;
      }

    }
    setTimeout(() => {
      this.loadView = true;
    }, 500);
  }

  ngOnInit() {
  }

  ngOnDestroy() {
    if (this.routerLinkBase !== 'tasks') {
      localStorage.removeItem('selected_browse_task');
      localStorage.removeItem('search_on');
      localStorage.removeItem('last_search_term');
      localStorage.removeItem('last_search_location');
      localStorage.removeItem('last_search_price');
      localStorage.removeItem('last_search_results');
    } else if (this.routerLinkBase !== 'my-tasks') {
      localStorage.removeItem('selected_my_task');
    }
  }
  async applySearch() {
    this.searchOn = true;
    localStorage.setItem('search_on', btoa(JSON.stringify(this.searchOn)));
    localStorage.setItem('last_search_term', btoa(JSON.stringify(this.searchSummary)));
    if (this.searchSummary && this.searchSummary.length > 0) {
      await this.taskService.searchbysummmary(this.searchSummary.toLowerCase()).subscribe(data => {
        // do stuff with our data here.
        if (data && (data.length > 0)) {
          this.tasks = data;
          this.selectedTask = this.tasks['0'];
          this.showTaskDetails = true;
          this.tasksAvailable = true;
          localStorage.setItem('last_search_results', btoa(JSON.stringify(data)));
          localStorage.setItem('search_on', btoa(JSON.stringify(true)));
          this.routeSearch('summary=' + this.searchSummary);
        } else {
          // What to do
          this.tasks = false;
          this.selectedTask = null;
          this.showTaskDetails = false;
          this.tasksAvailable = false;
          localStorage.setItem('search_on', btoa(JSON.stringify(true)));
          this.routeSearch('summary=' + this.searchSummary);
        }
      });
    } else {
        localStorage.removeItem('selected_browse_task');
        localStorage.removeItem('last_page');
        this.loadBrowseTasks();
    }

  }

  routeSearch(item): void {
    this.router.navigateByUrl('/tasks/search?' + item);
  }

  filterMyTasks(tasks): void {
    this.toggle = !this.toggle;
console.log({tasks});
  }


  viewSearchOptions(option): void {

if (this.mobileQuery.matches) {
this.dialogleft = '30px';
this.dialogtop = '100px';
}


    localStorage.removeItem('last_search_term');
    localStorage.removeItem('last_search_location');
    localStorage.removeItem('last_search_price');
    localStorage.removeItem('last_search_tasktype');
    localStorage.removeItem('last_search_results');
    localStorage.removeItem('selected_browse_task');
    if (option === 'location') {
      const dialogRef = this.dialog.open(LocationSearchComponent, {
        width: '300px',
        height: '200px',
        position: { top: this.dialogtop ? this.dialogtop : '100px', left: this.dialogleft ? this.dialogleft : '30px' }
      });
      dialogRef.afterClosed().subscribe(result => {
        if (result && (result['0'].length > 0)) {
          this.tasks = result['0'];
          this.selectedTask = this.tasks['0'];
          this.searchOn = true;
          this.showTaskDetails = true;
          this.tasksAvailable = true;
          localStorage.setItem('search_on', btoa(JSON.stringify(this.searchOn)));
          localStorage.setItem('last_search_location', btoa(JSON.stringify(result['1'])));
          localStorage.setItem('last_search_results', btoa(JSON.stringify(result['0'])));
          this.locationLabel = result['1'];
          this.routeSearch('location=' + result['1']);
        } else {
          if (result && result['1']) {
            this.locationLabel = result['1'];
            this.routeSearch('location=' + result['1']);
            localStorage.setItem('last_search_location', btoa(JSON.stringify(result['1'])));
          }
                    // What to do
                    this.tasks = false;
                    this.selectedTask = null;
                    this.showTaskDetails = false;
                    this.tasksAvailable = false;
        }
        localStorage.setItem('search_on', btoa(JSON.stringify(true)));
      });

    }

    if (option === 'price') {
      const dialogRef = this.dialog.open(PriceSearchComponent, {
        width: '300px',
        height: '200px',
        position: { top: this.dialogtop ? this.dialogtop : '100px', left: this.dialogleft ? this.dialogleft : '176px'  }
      });
      dialogRef.afterClosed().subscribe(result => {
        if (result && (result['0'].length > 0)) {
          this.tasks = result['0'];
          this.selectedTask = this.tasks['0'];
          this.searchOn = true;
          this.showTaskDetails = true;
          this.tasksAvailable = true;
          localStorage.setItem('search_on', btoa(JSON.stringify(this.searchOn)));
          localStorage.setItem('last_search_price', btoa(JSON.stringify(result['1'])));
          localStorage.setItem('last_search_results', btoa(JSON.stringify(result['0'])));
          this.priceLabel = result['1'];
          this.routeSearch('price=' + result['1']);
        } else {
          if (result && result['1']) {
            this.priceLabel = result['1'];
            this.routeSearch('price=' + result['1']);
            localStorage.setItem('last_search_price', btoa(JSON.stringify(result['1'])));
          }
                    // What to do
                    this.tasks = false;
                    this.selectedTask = null;
                    this.showTaskDetails = false;
                    this.tasksAvailable = false;
        }
        localStorage.setItem('search_on', btoa(JSON.stringify(true)));
      });

    }

    if (option === 'tasktype') {
      const dialogRef = this.dialog.open(TaskTypeSearchComponent, {
        width: '300px',
        height: '180px',
        position: { top: this.dialogtop ? this.dialogtop : '100px', left: this.dialogleft ? this.dialogleft : '295px'}
      });
      dialogRef.afterClosed().subscribe(result => {
        if (result && (result['0'].length > 0)) {
          this.tasks = result;
          this.selectedTask = this.tasks['0'];
        }
      });

    }

  }



  viewTask(task) {
    if (this.routerLinkBase === 'tasks') {
      localStorage.setItem('selected_browse_task', btoa(JSON.stringify(task)));
    } else if (this.routerLinkBase === 'my-tasks') {
      localStorage.setItem('selected_my_task', btoa(JSON.stringify(task)));
    }
    localStorage.setItem('last_page', '/' + this.routerLinkBase + '/' + task.Id);
    this.selectedTask = task;
    this.showTaskDetails = true;
  }

  async loadBrowseTasks() {
    this.queryOptions.pageNumber = 1;
    this.queryOptions.pageSize = 100;
    await this.taskService.list(this.queryOptions).subscribe(data => {
      // do stuff with our data here.
      this.tasks = data;
      if (this.tasks && this.tasks['0']) {
        if (localStorage.getItem('selected_browse_task')) {
          this.selectedTask = JSON.parse(atob(localStorage.getItem('selected_browse_task')));
        } else {
          this.selectedTask = this.tasks['0'];
        }
        this.showTaskDetails = true;
        this.tasksAvailable = true;
      }

      if (this.tasks.length < 1) {
        this.tasksAvailable = false;
      }

    });
  }

  async loadAssignedTasks() {
    //
    await this.taskService.listbyassigneduserid(this.user_sub).subscribe(data => {
      // do stuff with our data here.

      if (data && data['0']) {
        this.assignedTasks = data;
        this.selectedTask = data['0'];
        this.showAssignedTasksDetails = true;
        this.showTaskDetails = true;
        this.assignedTasksAvailable = true;
        this.tasksAvailable = true;
      }

    });


  }

  async loadPostedTasks() {
    //
    await this.taskService.listbycreatorid(this.user_sub).subscribe(data => {
      // do stuff with our data here.
      if (data && data['0']) {
        this.postedTasks = data;
        this.selectedTask = this.postedTasks['0'];
        this.showPostedTasksDetails = true;
        this.showTaskDetails = true;
        this.postedTasksAvailable = true;
        this.tasksAvailable = true;
      }

    });

  }

  async loadDraftTasks() {
    await this.taskService.listbycreatorid(this.user_sub).subscribe(data => {
      // do stuff with our data here.
      if (data && data['0']) {
        this.draftTasks = data;
        this.selectedTask = this.draftTasks['0'];
        this.showDraftTasksDetails = true;
        this.showTaskDetails = true;
        this.draftTasksAvailable = true;
        this.tasksAvailable = true;
      }
      if (this.draftTasks.length < 1) {
        this.draftTasksAvailable = false;
      }
    });
  }

  async getTasksFromOffers(myoffers) {
    // for each offer find a correspoding task with taskid
    for (let i = 0; i < myoffers.length; i++) {
      await this.taskService.read(myoffers[i].TaskId).subscribe(data => {
        // do stuff with our data here.
        if (data && (data.AssignedUser === ' ' || !data.AssignedUser)) {
          // populate pending tasks array
          this.pendingTasks[i] = data;
          this.showPendingTasksDetails = true;
          this.showTaskDetails = true;
          this.pendingTasksAvailable = true;
        }
      });
    }
  }

  async loadPendingTasks() {
    let myoffers;
    // find all offers with creator id match
    await this.offerService.listbycreatorid(this.user_sub).subscribe(data => {
      // do stuff with our data here.
      myoffers = data;
      if (myoffers) {
        this.getTasksFromOffers(myoffers);
        this.pendingTasks = data;
      }
    });
  }

  async loadCompletedTasks() {
    //
    await this.taskService.listbycreatorid(this.user_sub).subscribe(data => {
      // do stuff with our data here.
      if (data && data['0']) {
        this.completedTasks = data;
        this.selectedTask = this.completedTasks['0'];
        this.showCompletedTasksDetails = true;
        this.showTaskDetails = true;
        this.completedTasksAvailable = true;
      }
      if (this.completedTasks.length < 1) {
        this.completedTasksAvailable = false;
        this.tasksAvailable = true;
      }
    });
  }
}
