import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { ResourceService } from './resource.service';
import { Question } from '../models/question';
import { QuestionSerializer } from './question.serializer';
@Injectable({
  providedIn: 'root'
})
  export class QuestionService extends ResourceService<Question> {
    constructor(httpClient: HttpClient) {
      super(
        httpClient,
        environment.apiUrl,
        'questions',
        new QuestionSerializer());
    }
  }
