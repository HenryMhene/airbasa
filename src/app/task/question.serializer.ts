import { Question } from '../models/question';
import { MomentModule, DateFormatPipe } from 'ngx-moment';
export class QuestionSerializer {
    fromJson(json: any): Question {
      const question = new Question();
      question.Id = json.Id;
      question.TaskId = json.TaskId;
      question.CreatorId = json.CreatorId;
      question.CreatorName = json.CreatorName;
      question.CreatorPicture = json.CreatorPicture;
      question.Summary = json.Summary;
      question.Active = json.Active;
      question.CreatedAt = json.CreatedAt;
      const createdDate = new Date(json.CreatedAt * 1000);
      question.TempCreatedAt = (new DateFormatPipe()).transform(createdDate, 'YYYY-MM-DD HH:mm');
      return question;
    }

    toJson(question: Question): any {
      let CreatedAt: any;

      CreatedAt = question.CreatedAt;
      if (!question.Id || !question.CreatedAt) {
        CreatedAt = Math.floor(Date.now() / 1000);
      }
      return {
        Id: question.Id,
        TaskId: question.TaskId,
        CreatorId: question.CreatorId,
        CreatorName: question.CreatorName,
        CreatorPicture: question.CreatorPicture,
        Summary: question.Summary,
        Active: question.Active,
        CreatedAt: CreatedAt,
      };
    }
  }
