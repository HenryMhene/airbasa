import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { Task } from '../../models/task';
import { MatDialog, MatSnackBar } from '@angular/material';
import { LoginComponent } from '../../login/login.component';
import { OfferComponent } from '../offer/offer.component';
import { QueryOptions } from '../query.options';
import { OfferService } from '../offer.service';
import { Offer } from 'src/app/models/offer';
import { Question } from 'src/app/models/question';
import { v4 as uuid } from 'uuid';
import { QuestionService } from '../question.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AssignComponent } from '../assign/assign.component';
import { forEach } from '@angular/router/src/utils/collection';
import { ShareButtons } from '@ngx-share/core';
import { ReplyComponent } from '../reply/reply.component';
import { ReplyService } from '../reply.service';
import { EditComponent } from '../question/edit/edit.component';
import { MediaMatcher } from '@angular/cdk/layout';
import { MomentModule, DateFormatPipe } from 'ngx-moment';
import { ChangeDetectorRef } from '@angular/core';
@Component({
  selector: 'app-task-view',
  templateUrl: './task.view.component.html',
  styleUrls: ['./task.view.component.css']
})
export class TaskViewComponent implements OnInit, OnChanges {
  mobileQuery: MediaQueryList;
  private _mobileQueryListener: () => void;
  user_name = localStorage.getItem('profile_name');
  user_picture = localStorage.getItem('profile_picture');
  user_id_token = localStorage.getItem('id_token');
  user_access_token = localStorage.getItem('access_token');
  user_sub = localStorage.getItem('profile_sub');
  @Input() task: Task;
  offers: any;
  questions: any;
  offerReplys: any;
  showOfferDetails = false;
  showQuestionDetails = false;
  selectedOffer: Offer;
  offersAvailable = true;
  questionsAvailable = true;
  starsCount = 2.5;
  model = new Question();
  currentDatetime: any;
  questionloading = false;
  taskViewloading = false;
  hasAlreadyAskedQuestion = false;
  hasAlreadyMadeAnOffer = false;
  questionFormGroup: FormGroup;
  asignedoffer: any;
  loading = false;
  questionRepliesLoaded = false;
  offerRepliesLoaded = false;
  taskCreatedDate: any;
  constructor(public dialog: MatDialog, private offerService: OfferService, private queryOptions: QueryOptions
    , private questionService: QuestionService, public snackBar: MatSnackBar, private _formBuilder: FormBuilder,
    public share: ShareButtons, private replyService: ReplyService, media: MediaMatcher ,
    changeDetectorRef: ChangeDetectorRef) {

      this.mobileQuery = media.matchMedia('(max-width: 600px)');
      this._mobileQueryListener = () => changeDetectorRef.detectChanges();
      this.mobileQuery.addListener(this._mobileQueryListener);

    dialog.afterAllClosed
      .subscribe(() => {
        this.taskViewloading = true;
        if (this.task && this.task.Id) {
          this.loadOffers();
          this.loadQuestions();
        }

        setTimeout(() => {
          // if (this.offerRepliesLoaded && this.questionRepliesLoaded) {
            this.taskViewloading = false;
          // }
        }, 500);

      }
      );
  }

  async ngOnChanges(changes: SimpleChanges) {
    // changes.prop contains the old and the new value...
    this.taskViewloading = true;
    if (this.task && this.task.Id) {
      this.loadOffers();
      this.loadQuestions();
    }
    setTimeout(() => {
      // if (this.offerRepliesLoaded && this.questionRepliesLoaded) {
        this.taskViewloading = false;
      // }
    }, 500);
  }

  ngOnInit() {
    this.currentDatetime = Math.floor(Date.now() / 1000);
    const createdDate = new Date(this.task.CreatedAt * 1000);
    this.taskCreatedDate = (new DateFormatPipe()).transform(createdDate, 'YYYY-MM-DD HH:mm');
    this.questionFormGroup = this._formBuilder.group({
      secondCtrl: ['', Validators.required]
    });
  }

  async loadOffers() {
    await this.offerService.listbytaskid(this.task.Id).subscribe(data => {
      // do stuff with our data here.
      this.offers = data;

      // if task accepted remove the rest from list
      if (this.task.TaskStatus === 'Assigned' || this.task.TaskStatus === 'Completed') {
        for (let i = 0; i < this.offers.length; i++) {
          if (this.offers[i].CreatorId = this.task.AssignedUser) {
            this.asignedoffer = this.offers[i];
            this.offers = [];
            this.offers['0'] = this.asignedoffer;
            break;
          }
        }
      }
      if (this.offers && this.offers['0']) {
        this.selectedOffer = this.offers['0'];
        this.showOfferDetails = true;

        for (let i = 0; i < this.offers.length; i++) {

          if (this.offers[i].CreatorId === this.user_sub) {
            this.hasAlreadyMadeAnOffer = true;
          }
          this.loadOfferReplies(this.offers[i], i);
        }
      } else {
        this.offerRepliesLoaded = true;
      }
      if (this.offers.length < 1) {
        this.offersAvailable = false;
      }
    });
  }

  async loadOfferReplies(offer, position) {
    await this.replyService.listbyofferid(offer.Id).subscribe(data => {
      this.offers[position].Replies = data;
      this.offerRepliesLoaded = true;
      if (this.questionRepliesLoaded) {
        this.taskViewloading = false;
      }
    });
  }

  async loadQuestionReplies(question, position) {
    await this.replyService.listbyquestionid(question.Id).subscribe(data => {
      this.questions[position].Replies = data;
      this.questionRepliesLoaded = true;
      if (this.offerRepliesLoaded) {
        this.taskViewloading = false;
      }
    });
  }

  async loadQuestions() {
    await this.questionService.listbytaskid(this.task.Id).subscribe(data => {
      // do stuff with our data here.
      this.questions = data;
      if (this.questions && this.questions['0']) {
        this.showOfferDetails = true;

        for (let i = 0; i < this.questions.length; i++) {

          if (this.questions[i].CreatorId === this.user_sub) {
            this.hasAlreadyAskedQuestion = true;
          }
          this.loadQuestionReplies(this.questions[i], i);

        }

      } else {
        this.questionRepliesLoaded = true;
      }
      if (this.questions.length < 1) {
        this.questionsAvailable = false;
      }
    });
  }

  makeOffer(): void {
    if (this.user_access_token && this.user_id_token) {
      const dialogRef = this.dialog.open(OfferComponent, {
        width: '500px',
        data: { taskId: this.task.Id, taskPrice: this.task.Price, task: this.task }
      });
      dialogRef.afterClosed().subscribe(result => {
        console.log('The dialog was closed');
      });
    } else {
      const dialogRef = this.dialog.open(LoginComponent, {
        data: { name: 'test', animal: 'test' }
      });
      dialogRef.afterClosed().subscribe(result => {
        console.log('The dialog was closed');
      });

    }
  }

  reply(data): void {
    if (this.user_access_token && this.user_id_token) {
      const dialogRef = this.dialog.open(ReplyComponent, {
        width: '500px',
        height: '500px',
        data: { task: this.task, model: data }
      });
      dialogRef.afterClosed().subscribe(result => {
        console.log('The dialog was closed');
      });
    } else {
      const dialogRef = this.dialog.open(LoginComponent, {
        data: { name: 'test', animal: 'test' }
      });
      dialogRef.afterClosed().subscribe(result => {
        console.log('The dialog was closed');
      });

    }
  }

  assign(offer): void {
    const dialogRef = this.dialog.open(AssignComponent, {
      width: '350px',
      data: { taskId: this.task.Id, taskPrice: this.task.Price, offer: offer, task: this.task }
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

  editQuestion(question): void {
    const dialogRef = this.dialog.open(EditComponent, {
      width: '500px',
      data: { taskId: this.task.Id, taskPrice: this.task.Price, question: question, task: this.task }
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

  editOffer(offer): void {
    const dialogRef = this.dialog.open(OfferComponent, {
      width: '500px',
      data: { taskId: this.task.Id, taskPrice: this.task.Price, offer: offer, task: this.task }
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }



  async saveQuestion() {
    this.questionloading = true;
    this.model.Id = uuid();
    this.model.TaskId = this.task.Id;
    this.model.CreatorName = this.user_name;
    this.model.CreatorId = this.user_sub;
    this.model.CreatorPicture = this.user_picture;
    this.model.Active = 'true';
    this.model.CreatedAt = String(this.currentDatetime);

    await this.questionService.create(this.model).subscribe(data => {
      this.snackBar.open('Question sent!', 'Close', {
        duration: 2000,
      });

      setTimeout(() => {
        this.questionloading = false;
        this.loadQuestions();
      }, 500);

    });
  }

  login(): void {
    const dialogRef = this.dialog.open(LoginComponent, {
      data: { name: 'test', animal: 'test' }
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }


}
