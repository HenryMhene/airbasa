import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';
import { TaskService } from '../task.service';
import { Task } from 'src/app/models/task';
import { Offer } from 'src/app/models/offer';

export interface DialogData {
  taskId: string;
  taskPrice: string;
  offer: Offer;
  task: Task;
}

@Component({
  selector: 'app-assign',
  templateUrl: './assign.component.html',
  styleUrls: ['./assign.component.css']
})

export class AssignComponent implements OnInit {
  user_name = localStorage.getItem('profile_name');
  user_picture = localStorage.getItem('profile_picture');
  user_id_token = localStorage.getItem('id_token');
  user_access_token = localStorage.getItem('access_token');
  user_sub = localStorage.getItem('profile_sub');
  loading = false;
  constructor(
    public dialogRef: MatDialogRef<AssignComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    private taskService: TaskService,
    public snackBar: MatSnackBar
  ) { }

  ngOnInit() {
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  async assign() {
    this.loading = true;
    this.data.task.TaskStatus = 'Assigned';
    this.data.task.AssignedUser = this.data.offer.CreatorId;
    await this.taskService.update(this.data.task).subscribe(data => {
      this.snackBar.open('Task assigned!', 'Close', {
        duration: 2000,
      });
      this.loading = false;
      this.dialogRef.close();

    });
  }
}
