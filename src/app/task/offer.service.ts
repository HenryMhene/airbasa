import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { ResourceService } from './resource.service';
import { Offer } from '../models/offer';
import { OfferSerializer } from './offer.serializer';
@Injectable({
  providedIn: 'root'
})
  export class OfferService extends ResourceService<Offer> {
    constructor(httpClient: HttpClient) {
      super(
        httpClient,
        environment.apiUrl,
        'offers',
        new OfferSerializer());
    }
  }
