import { Task } from '../models/task';
import { MomentModule, DateFormatPipe } from 'ngx-moment';
export class TaskSerializer {
    fromJson(json: any): Task {
     if (json && json.Id) {
      const task = new Task();
      task.Id = json.Id;
      task.CreatorId = json.CreatorId;
      task.CreatorName = json.CreatorName;
      task.CreatorPicture = json.CreatorPicture;
      task.Summary = json.Summary;
      task.Description = json.Description;
      task.Price = json.Price;
      task.TaskLocation = atob(json.TaskLocation);
      const dueDate = new Date(json.DueDate * 1000);
      task.DueDate = (new DateFormatPipe()).transform(dueDate, 'YYYY-MM-DD HH:mm');
      task.TaskStatus = json.TaskStatus;
      task.Category = json.Category;
      task.NumberOfOffers = json.NumberOfOffers;
      task.Active = json.Active;
      task.MustHaves = json.MustHaves;
      task.AssignedUser = json.AssignedUser;
      task.CreatedAt = json.CreatedAt;
      return task;
     }
    }

    toJson(task: Task): any {
      let DueDate: any;
      let CreatedAt: any;
      DueDate = new Date(task.DueDate).getTime() / 1000;

      CreatedAt = task.CreatedAt;
      if (!task.Id || !task.CreatedAt) {
        CreatedAt = Math.floor(Date.now() / 1000);
      }
      if (isNaN(DueDate)) {
        DueDate = task.DueDate;
      }
      if (!task.NumberOfOffers) {
        task.NumberOfOffers = '0';
      }
      if (!task.AssignedUser) {
        task.AssignedUser = ' ';
      }
      return {
        Id: task.Id,
        CreatorId: task.CreatorId,
        CreatorName: task.CreatorName,
        CreatorPicture: task.CreatorPicture,
        Summary: task.Summary.toLowerCase(),
        Description: task.Description,
        Price: task.Price,
        TaskLocation: btoa(task.TaskLocation),
        DueDate: DueDate,
        TaskStatus: task.TaskStatus,
        Category: task.Category,
        NumberOfOffers: task.NumberOfOffers,
        Active: task.Active,
        AssignedUser: task.AssignedUser,
        MustHaves: task.MustHaves,
        CreatedAt: CreatedAt,
      };
    }
  }
