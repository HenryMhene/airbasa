import { HttpClient } from '@angular/common/http';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Resource } from '../models/Resource';
import { Serializer } from './serializer';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';
import { QueryOptions } from './query.options';
import { Injectable } from '@angular/core';
import { AuthService} from '../auth/auth.service';

export class ResourceService<T extends Resource> {
    constructor(
        private httpClient: HttpClient,
        private url: string,
        private endpoint: string,
        private serializer: Serializer) {}

      public create(item: T): Observable<T> {
        return this.httpClient
          .post<T>(`${this.url}/${this.endpoint}`, this.serializer.toJson(item))
          .map(data => this.serializer.fromJson(data) as T);
      }

      // public update(item: T): Observable<T> {
      //   return this.httpClient
      //     .put<T>(`${this.url}/${this.endpoint}/${item.Id}`,
      //       this.serializer.toJson(item))
      //     .map(data => this.serializer.fromJson(data) as T);
      // }

      public update(item: T): Observable<T> {
        return this.httpClient
          .put<T>(`${this.url}/${this.endpoint}`,
            this.serializer.toJson(item))
          .map(data => this.serializer.fromJson(data) as T);
      }

      read(id: string): Observable<T> {
        return this.httpClient
          .get(`${this.url}/${this.endpoint}/${id}`)
          .map((data: any) => this.serializer.fromJson(data.goods[0]) as T);
      }

      list(queryOptions: QueryOptions): Observable<T[]> {
        return this.httpClient
          .get(`${this.url}/${this.endpoint}?${queryOptions.toQueryString()}`)
          .map((data: any) => this.convertData(data.goods));
      }

      listbytaskid(taskId): Observable<T[]> {
        return this.httpClient
          .get(`${this.url}/${this.endpoint}/getbytaskid/${taskId}`)
          .map((data: any) => this.convertData(data.goods));
      }


      listbyofferid(Id): Observable<T[]> {
        return this.httpClient
          .get(`${this.url}/${this.endpoint}/getbyofferid/${Id}`)
          .map((data: any) => this.convertData(data.goods));
      }

      listbyquestionid(Id): Observable<T[]> {
        return this.httpClient
          .get(`${this.url}/${this.endpoint}/getbyquestionid/${Id}`)
          .map((data: any) => this.convertData(data.goods));
      }

      listbyassigneduserid(assignedUserId): Observable<T[]> {
        return this.httpClient
          .get(`${this.url}/${this.endpoint}/getbyassigneduser/${assignedUserId}`)
          .map((data: any) => this.convertData(data.goods));
      }

      listbycreatorid(creatorId): Observable<T[]> {
        return this.httpClient
          .get(`${this.url}/${this.endpoint}/getbycreatorid/${creatorId}`)
          .map((data: any) => this.convertData(data.goods));
      }

      searchbyprice(price): Observable<T[]> {
        return this.httpClient
          .get(`${this.url}/${this.endpoint}/search/price/${price}`)
          .map((data: any) => this.convertData(data.goods));
      }

      searchbylocation(location): Observable<T[]> {
        return this.httpClient
          .get(`${this.url}/${this.endpoint}/search/location/${btoa(location)}`)
          .map((data: any) => this.convertData(data.goods));
      }

      searchbytasktype(type): Observable<T[]> {
        return this.httpClient
          .get(`${this.url}/${this.endpoint}/search/status/${type}`)
          .map((data: any) => this.convertData(data.goods));
      }

      searchbysummmary(summary): Observable<T[]> {
        return this.httpClient
          .get(`${this.url}/${this.endpoint}/search/summary/${summary}`)
          .map((data: any) => this.convertData(data.goods));
      }


      delete(id: number) {
        return this.httpClient
          .delete(`${this.url}/${this.endpoint}/${id}`);
      }

      private convertData(data: any): T[] {
        return data.map(item => this.serializer.fromJson(item));
      }
    }

    @Injectable()
export class MyFirstInterceptor implements HttpInterceptor {

    constructor( public authService: AuthService) { }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
      let token  = null;
      if (localStorage.getItem('access_token')) {
         token = atob(localStorage.getItem('access_token'));
      } else {
          this.authService.clearProfile();
      }

        if (token) {
            req = req.clone({ headers: req.headers.set('Authorization', 'Bearer ' + token) });
        }

        if (!req.headers.has('Content-Type')) {
            req = req.clone({ headers: req.headers.set('Content-Type', 'application/json') });
        }

        req = req.clone({ headers: req.headers.set('Accept', 'application/json') });
        return next.handle(req);
    }
}
