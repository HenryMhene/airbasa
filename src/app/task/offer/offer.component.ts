import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Offer } from '../../models/offer';
import { OfferService } from '../offer.service';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { v4 as uuid } from 'uuid';
import { MatSnackBar } from '@angular/material';
import { Task } from 'src/app/models/task';
import { TaskService } from '../task.service';
import { MediaMatcher } from '@angular/cdk/layout';
import { MomentModule, DateFormatPipe } from 'ngx-moment';
import { ChangeDetectorRef } from '@angular/core';
export interface DialogData {
  taskId: string;
  taskPrice: string;
  offer: Offer;
  task: Task;
}

@Component({
  selector: 'app-offer',
  templateUrl: './offer.component.html',
  styleUrls: ['./offer.component.css']
})
export class OfferComponent implements OnInit {
  mobileQuery: MediaQueryList;
  private _mobileQueryListener: () => void;
  summaryFormControl = new FormControl('', [
    Validators.required
  ]);
  priceFormControl = new FormControl('', [
    Validators.min(1), Validators.max(50000)
  ]);
  user_name = localStorage.getItem('profile_name');
  user_picture = localStorage.getItem('profile_picture');
  user_id_token = localStorage.getItem('id_token');
  user_access_token = localStorage.getItem('access_token');
  user_sub = localStorage.getItem('profile_sub');
  loading = false;
  amountReceivable: number;
  serviceFee = 0;
  currentDatetime: any;
  offerAmount: any;
  model = new Offer();
  constructor(
    public dialogRef: MatDialogRef<OfferComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    private offerService: OfferService,
    private taskService: TaskService,
    public snackBar: MatSnackBar, media: MediaMatcher ,
    changeDetectorRef: ChangeDetectorRef
  ) {
    this.mobileQuery = media.matchMedia('(max-width: 600px)');
    this._mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addListener(this._mobileQueryListener);
   }

  ngOnInit() {

    this.offerAmount = this.data.taskPrice;

    this.serviceFee = (20 / 100) * this.offerAmount;

    this.serviceFee = Number(this.serviceFee.toFixed(2));
    if (this.data.offer) {
      this.model = this.data.offer;
      this.offerAmount = this.data.offer.Amount;
      this.amountReceivable = this.data.offer.Receivable;
      this.currentDatetime = Math.floor(Date.now() / 1000);

    } else {
      this.amountReceivable = (this.offerAmount - this.serviceFee);
      this.currentDatetime = Math.floor(Date.now() / 1000);
    }

  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  async makeOffer() {
    this.loading = true;

if (this.model.Id) {
  this.model.Amount = this.offerAmount;
  this.model.ServiceFee = this.serviceFee;
  this.model.Receivable = this.amountReceivable;
  await this.offerService.update(this.model).subscribe(data => {
    this.snackBar.open('Offer Updated!', 'Close', {
      duration: 2000,
    });
    this.loading = false;
    this.dialogRef.close();

  });

} else {
  this.model.Id = uuid();
  this.model.TaskId = this.data.taskId;
  this.model.Amount = this.offerAmount;
  this.model.CreatorName = this.user_name;
  this.model.CreatorId = this.user_sub;
  this.model.CreatorPicture = this.user_picture;
  this.model.ServiceFee = this.serviceFee;
  this.model.Receivable = this.amountReceivable;
  this.model.Active = 'true';
  this.model.CreatedAt = String(this.currentDatetime);

  await this.offerService.create(this.model).subscribe(data => {
  });

  this.data.task.NumberOfOffers = String(Number(this.data.task.NumberOfOffers) + 1);
  await this.taskService.update(this.data.task).subscribe(taskdata => {
    this.snackBar.open('Offer Placed!', 'Close', {
      duration: 2000,
    });
    this.loading = false;
    this.dialogRef.close();
    });
}


  }

  offerAmountChange(data): void {
    this.offerAmount = data.target.value;

    this.serviceFee = (20 / 100) * this.offerAmount;

    this.amountReceivable = (this.offerAmount - this.serviceFee);
  }
}
