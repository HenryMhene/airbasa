import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { ResourceService } from './resource.service';
import { ReplySerializer } from './reply.serializer';
import { Reply } from '../models/reply';
@Injectable({
  providedIn: 'root'
})
  export class ReplyService extends ResourceService<Reply> {
    constructor(httpClient: HttpClient) {
      super(
        httpClient,
        environment.apiUrl,
        'replys',
        new ReplySerializer());
    }
  }
