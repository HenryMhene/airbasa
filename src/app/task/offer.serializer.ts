import { Offer } from '../models/offer';
import { MomentModule, DateFormatPipe } from 'ngx-moment';
export class OfferSerializer {
    fromJson(json: any): Offer {
      const offer = new Offer();
      offer.Id = json.Id;
      offer.TaskId = json.TaskId;
      offer.CreatorId = json.CreatorId;
      offer.CreatorName = json.CreatorName;
      offer.CreatorPicture = json.CreatorPicture;
      offer.Summary = json.Summary;
      offer.Status = json.Status;
      offer.Amount = json.Amount;
      offer.ServiceFee = json.ServiceFee;
      offer.Receivable = json.Receivable;
      offer.Active = json.Active;
      offer.CreatedAt = json.CreatedAt;
      const createdDate = new Date(json.CreatedAt * 1000);
      offer.TempCreatedAt = (new DateFormatPipe()).transform(createdDate, 'YYYY-MM-DD HH:mm');
      return offer;
    }

    toJson(offer: Offer): any {

      let CreatedAt: any;

      CreatedAt = offer.CreatedAt;
      if (!offer.Id || !offer.CreatedAt) {
        CreatedAt = Math.floor(Date.now() / 1000);
      }

      return {
        Id: offer.Id,
        TaskId: offer.TaskId,
        CreatorId: offer.CreatorId,
        CreatorName: offer.CreatorName,
        CreatorPicture: offer.CreatorPicture,
        Summary: offer.Summary,
        Status: offer.Status,
        Amount: offer.Amount,
        ServiceFee: offer.ServiceFee,
        Receivable: offer.Receivable,
        Active: offer.Active,
        CreatedAt: CreatedAt,
      };
    }
  }
