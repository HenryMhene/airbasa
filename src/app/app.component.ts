import { Component } from '@angular/core';
import { AuthService } from './auth/auth.service';
import {ActivatedRoute, NavigationEnd, Router} from '@angular/router';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  allowHeader = false;

  loadApp = false;

  constructor(public auth: AuthService, private router: Router, private route: ActivatedRoute) {
        // check the route
        this.router.events.subscribe(event => {
          if (event instanceof NavigationEnd) {
              if (this.route.root.firstChild) {
                this.allowHeader = this.route.root.firstChild.snapshot.data['allowHeader'];
              }

          }
    });

    setTimeout(() => {
      this.loadApp = true;
  }, 500);

  auth.handleAuthentication();
  }
}
