import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AccountComponent } from './account/account.component';
import { LoginComponent } from './login/login.component';
import { TaskListComponent } from './task/list/task.list.component';
import { LandingPageComponent } from './landing-page/landing-page.component';


const routes: Routes = [
  { path: 'account', component: AccountComponent, data: {allowHeader: true} },
  { path: 'account/:section', component: AccountComponent, data: {allowHeader: true} },
  { path: 'login', component: LoginComponent, data: {allowHeader: false}  },
  { path: 'tasks', component: TaskListComponent, data: {allowHeader: true}  },
  { path: 'tasks/:taskid', component: TaskListComponent, data: {allowHeader: true}  },
  { path: 'my-tasks', component: TaskListComponent, data: {allowHeader: true}  },
  { path: 'my-tasks/:taskid', component: TaskListComponent, data: {allowHeader: true}  },
  { path: 'landing', component: LandingPageComponent, data: {allowHeader: true}  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
