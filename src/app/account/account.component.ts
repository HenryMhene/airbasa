import { Component, OnInit } from '@angular/core';
import { AuthService } from './../auth/auth.service';
import {ChangeDetectorRef, OnDestroy} from '@angular/core';
import {MediaMatcher} from '@angular/cdk/layout';
import { Config, SidenavService } from './../sidenav/sidenav.service';
import {MatDialog} from '@angular/material';
import { CreateComponent } from '../task/create/create.component';
import {ActivatedRoute, Router} from '@angular/router';
@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.css']
})

export class AccountComponent implements OnDestroy {
  error: any;
  headers: string[];
  config: Config;
  mobileQuery: MediaQueryList;
  user_name = localStorage.getItem('profile_name');
  user_picture = localStorage.getItem('profile_picture');

  fillerNavResponse: any;

  private _mobileQueryListener: () => void;

  section = 'dashboard';

  sections = [
    {id: 'dashboard', label: 'Dashboard', icon: 'dashboard', link: '/account/dashboard'},
    {id: 'payments_history', label: 'Payments history', icon: 'attach_money', link: '/account/payments_history'},
    {id: 'payment_methods', label: 'Payment methods', icon: 'credit_card', link: '/account/payment_methods'},
    {id: 'notifications', label: 'Notifications', icon: 'notifications_none', link: '/account/notifications'},
    {id: 'refer_a_friend', label: 'Refer a friend', icon: 'sentiment_very_satisfied', link: '/account/refer_a_friend'},
    {id: 'settings', label: 'Settings', icon: 'settings', link: '/settings'}
];


  constructor(
    public dialog: MatDialog,
    public auth: AuthService,
    changeDetectorRef: ChangeDetectorRef,
    media: MediaMatcher,
    private configService: SidenavService,
    private route: ActivatedRoute,
    private router: Router
  ) {

    this.route.params.subscribe(params => {
      if (params['section'] > '') {
          this.section = params['section'];
      }

  });



    this.mobileQuery = media.matchMedia('(max-width: 685px)');
    this._mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addListener(this._mobileQueryListener);
    this.fillerNavResponse = this.showConfig();

  }

  openTaskCreate(): void {
    const dialogRef = this.dialog.open(CreateComponent, {
      width: '500px',
       data: {name: 'test', animal: 'test'}
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

  openBrowseTasks(): void {
    this.router.navigateByUrl('/account/browse_tasks');
  }
  openMyTasks(): void {
    this.router.navigateByUrl('/account/my_tasks');

  }

  showConfig() {
     this.configService.getOptions()
      .subscribe((data: Config) => this.config = {
          heroesUrl: data['heroesUrl'],
          textfile:  data['textfile']
      });
  }


  logout(): void {
    this.auth.logout();
  }

  ngOnDestroy(): void {
    this.mobileQuery.removeListener(this._mobileQueryListener);
  }
}
