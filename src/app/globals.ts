// globals.ts
import { Injectable } from '@angular/core';

@Injectable()
export class Globals {
  email_verified = false;
  access_token: string;
  id_token: string;
  expires_at: string;
  profile_picture: string;
  profile_id: string;
  profile_sub: string;
}
