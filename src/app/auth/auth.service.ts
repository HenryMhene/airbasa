import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import * as auth0 from 'auth0-js';
import { JwtHelperService } from '@auth0/angular-jwt';
import { environment } from '../../environments/environment';
import { Globals } from '../globals';
(window as any).global = window;

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  auth0 = new auth0.WebAuth({
    clientID: 'hd2iShq5KnTszlJ2LD1T8qMDWJB6YXos',
    domain: 'airbasa.au.auth0.com',
    responseType: 'token id_token',
    audience: 'https://airbasa.au.auth0.com/userinfo',
    redirectUri: environment.url,
    scope: 'openid profile'
  });


  public handleAuthentication(): void {

    if (localStorage.getItem('last_page') === '/landing') {
      localStorage.setItem('last_page', '/account');
    } else if (!localStorage.getItem('last_page')) {
      localStorage.setItem('last_page', '/account');
    }

    this.auth0.parseHash((err, authResult) => {
      if (authResult && authResult.accessToken && authResult.idToken) {
        window.location.hash = '';
        this.setSession(authResult);
        this.router.navigate([localStorage.getItem('last_page')]);
      } else if (err) {
        if (!this.isAuthenticated()) {
          // Remove tokens and expiry time from localStorage
          localStorage.removeItem('access_token');
          localStorage.removeItem('id_token');
          localStorage.removeItem('expires_at');
          localStorage.removeItem('profile_id');
          localStorage.removeItem('profile_name');
          localStorage.removeItem('profile_picture');
          localStorage.removeItem('profile_sub');
          this.router.navigate(['/landing']);
        }
      }

      if (!this.isAuthenticated()) {
        // Remove tokens and expiry time from localStorage
        localStorage.removeItem('access_token');
        localStorage.removeItem('id_token');
        localStorage.removeItem('expires_at');
        localStorage.removeItem('profile_id');
        localStorage.removeItem('profile_name');
        localStorage.removeItem('profile_picture');
        localStorage.removeItem('profile_sub');
        localStorage.removeItem('last_page');
        localStorage.removeItem('selected_browse_task');
        localStorage.removeItem('selected_my_task');
        this.router.navigate(['/landing']);
      } else {
        this.router.navigate([localStorage.getItem('last_page')]);
      }
    });
  }

  private setSession(authResult): void {
    // Set the time that the Access Token will expire at
    const expiresAt = JSON.stringify((authResult.expiresIn * 1000) + new Date().getTime());
    localStorage.setItem('access_token', btoa(authResult.idToken));
    localStorage.setItem('id_token', btoa(authResult.idToken));
    localStorage.setItem('expires_at', expiresAt);
    localStorage.setItem('profile_picture', authResult.idTokenPayload.picture);
    localStorage.setItem('profile_id', authResult.idTokenPayload.aud);
    localStorage.setItem('profile_sub', btoa(authResult.idTokenPayload.sub));

    if (authResult.idTokenPayload['http://first_name'] && authResult.idTokenPayload['http://last_name']) {
      localStorage.setItem('profile_name',
        authResult.idTokenPayload['http://first_name'] + ' ' + authResult.idTokenPayload['http://last_name']);
    } else {
      localStorage.setItem('profile_name', authResult.idTokenPayload.name);
    }
    this.globals.email_verified = authResult.idTokenPayload['http://email_verified'];
    this.globals.access_token = authResult.accessToken;
    this.globals.id_token = authResult.id_token;
    this.globals.expires_at = authResult.expires_at;
    this.globals.profile_picture = authResult.idTokenPayload.profile_picture;
    this.globals.profile_id = authResult.idTokenPayload.profile_id;
    this.globals.profile_sub = authResult.idTokenPayload.profile_sub;
  }

  public logout(): void {
    // Remove tokens and expiry time from localStorage
    localStorage.removeItem('access_token');
    localStorage.removeItem('id_token');
    localStorage.removeItem('expires_at');
    localStorage.removeItem('profile_sub');
    localStorage.removeItem('profile_id');
    localStorage.removeItem('profile_name');
    localStorage.removeItem('profile_picture');
    localStorage.removeItem('profile_sub');
    localStorage.removeItem('last_page');
    localStorage.removeItem('selected_browse_task');
    localStorage.removeItem('selected_my_task');
    localStorage.removeItem('search_on');
    localStorage.removeItem('last_search_term');
    localStorage.removeItem('last_search_results');
    // Go back to the home route
    this.router.navigate(['/landing']);
  }

  public clearProfile(): void {
    // Remove tokens and expiry time from localStorage
    localStorage.removeItem('access_token');
    localStorage.removeItem('id_token');
    localStorage.removeItem('expires_at');
    localStorage.removeItem('profile_sub');
    localStorage.removeItem('profile_id');
    localStorage.removeItem('profile_name');
    localStorage.removeItem('profile_picture');
    localStorage.removeItem('profile_sub');

    // this.auth0.logout();
  }

  public isAuthenticated(): boolean {
    // Check whether the current time is past the
    // Access Token's expiry time
    const expiresAt = JSON.parse(localStorage.getItem('expires_at') || '{}');
    return new Date().getTime() < expiresAt;
  }

  constructor(public router: Router, private globals: Globals) { }

  public login(): void {
    this.auth0.authorize();
  }
}
