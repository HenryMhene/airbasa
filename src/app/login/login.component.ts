import { Component, OnInit, Inject } from '@angular/core';
import {environment} from '../../environments/environment';
import { Auth0Lock } from 'auth0-lock';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Router } from '@angular/router';
export interface DialogData {
    IncomingUrl: string;
    name: string;
  }
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<LoginComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    public router: Router
  ) { }

  ngOnInit() {
    localStorage.setItem('last_page', this.router.url);
    const options = {
        additionalSignUpFields: [{
            name: 'first_name',
            placeholder: 'your first name',
            // The following properties are optional
             icon: 'https://s3-ap-southeast-2.amazonaws.com/afritasks/public/iconmonstr-pencil-thin-16.png',
            validator: function(first_name) {
              return {
                 valid: first_name.length >= 2,
                 hint: 'Must have 2 or more chars' // optional
              };
            }
          },
          {
            name: 'last_name',
            placeholder: 'your last name',
            icon: 'https://s3-ap-southeast-2.amazonaws.com/afritasks/public/iconmonstr-pencil-thin-16.png',
            validator: function(last_name) {
              return {
                 valid: last_name.length >= 2,
                 hint: 'Must have 2 or more chars' // optional
              };
            }
          }],
        container: 'login-container',
        theme: {
            logo:            '',
            primaryColor:    '#2196f3'
        },
        allowSignUp: true,
        allowLogin: true,
        allowShowPassword: true,
        languageDictionary: {
            title: '...'
        },
        auth: {
            redirectUrl: environment.url,
            responseType: 'token id_token',
            sso: true,
            audience: 'https://airbasa.au.auth0.com/userinfo',
            params: {
                scope: 'openid profile'
            }
        },

    };

    // initialize
    const lock = new Auth0Lock('hd2iShq5KnTszlJ2LD1T8qMDWJB6YXos', 'airbasa.au.auth0.com', options);

    // render
    lock.show();

}

onNoClick(): void {
    this.dialogRef.close();
  }


}
