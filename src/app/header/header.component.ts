import { Component, OnInit } from '@angular/core';
import { AuthService } from './../auth/auth.service';
import { MatDialog, MAT_DIALOG_SCROLL_STRATEGY } from '@angular/material';
import { CreateComponent } from '../task/create/create.component';
import { MediaMatcher } from '@angular/cdk/layout';
import { Router } from '@angular/router';
import { ChangeDetectorRef } from '@angular/core';
import { LoginComponent } from '../login/login.component';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  user_name = localStorage.getItem('profile_name');
  user_picture = localStorage.getItem('profile_picture');
  user_id_token = localStorage.getItem('id_token');
  user_access_token = localStorage.getItem('access_token');

  mobileQuery: MediaQueryList;
  private _mobileQueryListener: () => void;
  constructor(
    public dialog: MatDialog,
    private router: Router,
    public auth: AuthService,
    media: MediaMatcher,
    changeDetectorRef: ChangeDetectorRef
  ) {
    this.mobileQuery = media.matchMedia('(max-width: 737px)');
    this._mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addListener(this._mobileQueryListener);
  }

  ngOnInit() {
  }

  openTaskCreate(): void {
    localStorage.removeItem('search_on');
    localStorage.removeItem('last_search_term');
    localStorage.removeItem('last_search_results');
    if (this.user_access_token && this.user_id_token) {
      const dialogRef = this.dialog.open(CreateComponent, {
        width: '500px',
        data: { name: 'test', animal: 'test' }
      });

      dialogRef.afterClosed().subscribe(result => {
        console.log('The dialog was closed');
      });
    } else {
      const dialogRef = this.dialog.open(LoginComponent, {
        data: { name: 'test', animal: 'test' }
      });
      dialogRef.afterClosed().subscribe(result => {
        console.log('The dialog was closed');
      });

    }
  }

  openLogin(): void {
    const dialogRef = this.dialog.open(LoginComponent, {
      data: { name: 'test', animal: 'test' },
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

  openBrowseTasks(): void {
    localStorage.removeItem('search_on');
    localStorage.removeItem('last_search_term');
    localStorage.removeItem('last_search_location');
    localStorage.removeItem('last_search_price');
    localStorage.removeItem('last_search_tasktype');
    localStorage.removeItem('last_search_results');
    localStorage.removeItem('selected_browse_task');
    if (localStorage.getItem('selected_browse_task')) {
      this.router.navigateByUrl('/tasks');
    } else if ((this.router.url.indexOf('/tasks') === 0)) {
      this.router.navigateByUrl('/tasks');
    } else {
      this.router.navigateByUrl('/tasks');
    }
  }
  openMyTasks(): void {
    localStorage.removeItem('search_on');
    localStorage.removeItem('last_search_term');
    localStorage.removeItem('last_search_location');
    localStorage.removeItem('last_search_price');
    localStorage.removeItem('last_search_tasktype');
    localStorage.removeItem('last_search_results');
    localStorage.removeItem('selected_browse_task');
    this.router.navigateByUrl('/my-tasks');
  }
  goToDashBoard(): void {
    localStorage.removeItem('search_on');
    localStorage.removeItem('last_search_term');
    localStorage.removeItem('last_search_location');
    localStorage.removeItem('last_search_price');
    localStorage.removeItem('last_search_tasktype');
    localStorage.removeItem('last_search_results');
    localStorage.removeItem('selected_browse_task');
    if (this.user_access_token) {
      this.router.navigateByUrl('/account');
    } else {
      this.router.navigateByUrl('/landing');
    }
  }
  logout(): void {
    this.user_name = null;
    this.user_picture = null;
    this.user_id_token = null;
    this.user_access_token = null;
    this.auth.logout();
  }
}
