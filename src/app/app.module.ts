import { BrowserModule } from '@angular/platform-browser';
import { NgModule} from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { ShareModule } from '@ngx-share/core';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import {MatButtonModule,
  MatCheckboxModule,
  MatToolbarModule,
  MatIconModule,
  MatMenuModule,
  MatTooltipModule,
  MatFormFieldModule,
  MatButtonToggleModule,
  MatSnackBarModule,
  MatProgressSpinnerModule,
  MatSliderModule,
  MatSlideToggleModule} from '@angular/material';
import {MatSidenavModule} from '@angular/material';
import {
  MatCardModule,
  MatChipsModule,
  MatDatepickerModule,
  MatDialogModule,
  MatGridListModule,
  MatInputModule,
  MatListModule,
  MatNativeDateModule,
  MatProgressBarModule,
  MatRadioModule,
  MatStepperModule,
  MatTableModule,
  MatTabsModule,
} from '@angular/material';
import { HttpClientModule, HTTP_INTERCEPTORS } from '../../node_modules/@angular/common/http';
import { LoginComponent } from './login/login.component';
import { AuthComponent } from './auth/auth.component';
import { CreateComponent } from './task/create/create.component';
import { OfferComponent } from './task/offer/offer.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MyTasksComponent } from './my-tasks/my-tasks.component';
import { PaymentsHistoryComponent } from './payments-history/payments-history.component';
import { PaymentMethodsComponent } from './payment-methods/payment-methods.component';
import { NotificationsComponent } from './notifications/notifications.component';
import { ReferAfriendComponent } from './refer-afriend/refer-afriend.component';
import { SettingsComponent } from './settings/settings.component';
import { AccountComponent } from './account/account.component';
import { HeaderComponent } from './header/header.component';
import { LandingPageComponent } from './landing-page/landing-page.component';
import { NguCarousel, NguCarouselModule } from '@ngu/carousel';
import { GooglePlaceModule } from 'ngx-google-places-autocomplete';
import { TaskListComponent } from './task/list/task.list.component';
import { TaskViewComponent } from './task/view/task.view.component';
import { LandingFixService } from './landing-page/landing-fix.service';
import { CommonModule } from '@angular/common';
import { IntroComponent } from './landing-page/intro/intro.component';
import { TasksCarouselComponent } from './landing-page/tasks-carousel/tasks-carousel.component';
import { CtaComponent } from './landing-page/cta/cta.component';
import { FooterComponent } from './landing-page/footer/footer.component';
import { Task } from './models/task';
import { QueryOptions } from './task/query.options';
import { MomentModule } from 'ngx-moment';
import {RatingModule} from 'ngx-rating';
import { AssignComponent } from './task/assign/assign.component';
import { CustomUrlSerializer } from './task/CustomUrlSerializer';
import { UrlSerializer } from '@angular/router';
import { MyFirstInterceptor } from './task/resource.service';
import { ReplyComponent } from './task/reply/reply.component';
import { EditComponent } from './task/question/edit/edit.component';
import {TimeAgoPipe} from 'time-ago-pipe';
import { LocationSearchComponent } from './search/location-search/location-search.component';
import { PriceSearchComponent } from './search/price-search/price-search.component';
import { TaskTypeSearchComponent } from './search/task-type-search/task-type-search.component';
import { Globals } from './globals';
@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    LoginComponent,
    AuthComponent,
    CreateComponent,
    TaskListComponent,
    MyTasksComponent,
    PaymentsHistoryComponent,
    PaymentMethodsComponent,
    NotificationsComponent,
    ReferAfriendComponent,
    SettingsComponent,
    AccountComponent,
    HeaderComponent,
    LandingPageComponent,
    IntroComponent,
    TasksCarouselComponent,
    FooterComponent,
    CtaComponent,
    TaskViewComponent,
    OfferComponent,
    AssignComponent,
    ReplyComponent,
    EditComponent, TimeAgoPipe, LocationSearchComponent, PriceSearchComponent, TaskTypeSearchComponent
  ],
  entryComponents: [CreateComponent, OfferComponent, AssignComponent, ReplyComponent,
    EditComponent, LocationSearchComponent, PriceSearchComponent, TaskTypeSearchComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    CommonModule,
    FormsModule,
    HttpClientModule,
    MatTableModule,
    MatTabsModule,
    MatDialogModule,
    MatStepperModule,
    MatFormFieldModule,
    MatInputModule,
    MatCardModule,
    MatProgressBarModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatCheckboxModule,
    MatToolbarModule,
    MatGridListModule,
    MatChipsModule,
    NguCarouselModule,
    GooglePlaceModule,
    MomentModule,
    MatRadioModule,
    MatButtonToggleModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatSnackBarModule,
    MatProgressSpinnerModule,
    RatingModule,
    ShareModule.forRoot(),
    FontAwesomeModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatIconModule, MatMenuModule, MatTooltipModule, MatSidenavModule, MatListModule
  ],
  providers: [
    Globals,
    LandingFixService,
    Task,
    QueryOptions,
    { provide: UrlSerializer, useClass: CustomUrlSerializer },
    {
      provide: HTTP_INTERCEPTORS, useClass: MyFirstInterceptor, multi: true,
  }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
