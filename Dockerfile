FROM node:8.11-alpine as builder
RUN apk update                                  && \
	apk add --no-cache git python make g++ bash	
RUN npm config set unsafe-perm true && npm i npm@latest -g && npm install -g @angular/cli
COPY . /app
WORKDIR /app

RUN npm install && npm run build-production

FROM nginx

ARG NODE_ENV=production
ENV NODE_ENV ${NODE_ENV}
COPY nginx.conf /etc/nginx/conf.d/default.conf
COPY --from=builder /app/dist/airbasa /usr/share/nginx/html









